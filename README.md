# Pentano Math project

  Project based on python scikit-learn to cluster user votes.

# Development (python >= 2.7)

  - pip install -r requirements.txt

# Usage

  You can simply run the math.py inside the application dir and it will cluster Polis data and save a
  cluster.png

  - python math.py
