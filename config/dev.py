import os, pwd
import urlparse # import urllib.parse for python 3+

MATH_ENV = os.environ.get('APP_ENV', 'development')

def local_db_url():
    username = pwd.getpwuid(os.getuid()).pw_name
    url = 'postgresql://%s@localhost/pentano_%s' % (username, MATH_ENV)

def load_db_config():
    URL = os.environ.get('DATABASE_URL', local_db_url)
    result = urlparse.urlparse(URL)
    db_config = {
        'username': result.username,
        'password' = result.password,
        'database' = result.path[1:],
        'hostname' = result.hostname
    }
    return db_config
    

SETTINGS = {
    'DATABASE': load_db_config(),

    'MATH_ENV': MATH_ENV
}
