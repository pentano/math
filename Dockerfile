FROM python:3.4-wheezy

ADD . /code
WORKDIR /code

CMD ["sleep", "infinity"]
