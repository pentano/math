#!/usr/bin/python

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from collections import Counter
from os import sys, makedirs, path

# handle target folder dir for saving results
target_dir = ''
if len(sys.argv) > 2:
    target_dir  = sys.argv[2]
    if not path.exists(target_dir):
        makedirs(target_dir)
    else:
        print("Overriding results in %s" % target_dir)

run_pca = True
if len(sys.argv) > 1:
    run_pca = sys.argv[1]
    run_pca = run_pca == "1"

print("Run pca = %s" % run_pca)

from sklearn.metrics import silhouette_score

import matplotlib.pyplot as plt
import csv
import numpy as np
import time

#Symbols and colors used by matplotlib
symbols = ['>','v','s','<','d', '*', 'x', 'p', 'H']
colors = ['b','g','r','c','m','y','g', 'm', 'k']

# file containing polis conversation
csv_file = 'data-examples/participants-votes.csv'

dataset = []
silhouette = 0
current_silhouette = -1
final_labels = [] = []
best_kmeans = None


# open polis file and get only the votes columns
# for this conversation there is 17 comments 
with open(csv_file, 'r') as f:
    buffer = csv.reader(f, delimiter=',', quotechar='|')
    for line in buffer:
        # get the 17 last itens from the current line in the csv
        votes = line[-17:]
        # cast votes to int and fill missing data with 0
        votes_cleaned = [int(vote) if vote != '' else 0 for vote in votes]
        dataset.append(votes_cleaned)

# remove the first line with columns descriptions
dataset.remove(dataset[0])

# create a numpy array with the data
dataset = np.array(dataset)

# create PCA instance, see docs for more paramenters
decomposition = PCA(n_components=2)

# reduces dimensionality
reduced_data = decomposition.fit_transform(dataset)

print("reduced data")
print(len(reduced_data))
current_time = time.time()

for k in range(2,5):
    plt.cla()

## cluster data using k clusters
    kmeans = KMeans(n_clusters=k, max_iter=300, verbose=0, random_state=0)

## calculate clusters and label for each datapoint
    kmeans_data = reduced_data

    labels = kmeans.fit_predict(kmeans_data)
    print("len labels = %s  " % len(set(labels))) 
    print(labels)
    if(set(labels) > 1):
        current_silhouette = silhouette_score(kmeans_data, labels)
        print("Silhueta calculada \n")

    if(current_silhouette >= silhouette):
        silhouette = current_silhouette
        best_kmeans =  kmeans
        print("Labels and silhouette for k = ", k)
        print(Counter(labels))
        print(current_silhouette)
    else:
        print("Aborting")
        print("Labels for k = ", k)
        print(Counter(labels))
        print(current_silhouette)
        pass


## data matrix containing the n clusters array to plot
data_matrix = []

## create an array in data_matrix for each cluster
for x in range(best_kmeans.n_clusters):
    data_matrix.append([])

# assign each datapoint to it's cluster array in data_matrix
for x in range(len(reduced_data)):
    data_index = best_kmeans.labels_[x]
    xx = reduced_data[x][0]
    yy = reduced_data[x][1]
    data_matrix[data_index].append([xx,yy])

## plot each cluster with different symbol notation
for index in range(len(data_matrix)):
    vis = symbols[index] + colors[index]
    x_axis = np.array(data_matrix[index])[:,0]
    y_axis = np.array(data_matrix[index])[:,1]
    plt.plot(x_axis, y_axis, vis)

centroids = best_kmeans.cluster_centers_
plt.plot(centroids[:, 0], centroids[:, 1], 'xk', linewidth=50, markersize=12)
plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
          'Centroids are marked with black X mark')

plt_filename = 'cluster-' + str(best_kmeans.n_clusters) + '.png'
plt.savefig(target_dir + plt_filename)
