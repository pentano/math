import psycopg2

import logging
logger = logging.getLogger(__name__)

class Postgres():
    def __init__(self, config):
        self.database = config.get('database')
        self.username = config.get('username')
        self.password = config.get('password')
        self.hostname = config.get('hostname')
        self.conn = self.connect()

    def connect(self):
        logger.info(' -> Connecting to database with following settings:')
        logger.info(' -> DATABASE SETTINGS: %s', self.__rpr__())
        try:
            connection = psycopg2.connect(
                database = self.database,
                user = self.username,
                password = self.password,
                host = self.hostname
            )

            logger.info('Connected to database')
            return(connection)
        except Exception:
            logger.exception('\n\n -> Could not connect to database')
            return None

    # E.g. poll('votes', 'created_at', 1495976727)
    def poll(table_name, marker_field, timestamp=0):
        print('Polling %s with %s up to %s' % (table_name, field, timestamp))

    def __rpr__(self):
        return {
            'Database': self.database,
            'Hostname': self.hostname
        }
