import time
import logging
logger = logging.getLogger(__name__)

from .postgres import Postgres

class Poller():
    def __init__(self, config, table_name, marker_field):
        self.timestamp = self.polling_timestamp(config)
        self.interval = self.polling_interval(config)
        self.table_name = table_name
        self.marker_field = marker_field
        self.db = Postgres(config)
        
    def get_data(self):
        return None

    def start(self):
        while(True):
            data = self.get_data()
            if(data != None):
                logger.info("Clustering Data")
                logger.info("Convo-update manager")
            else:
                time.sleep(self.interval)
                logger.info(" -> Polling %s. Timestamp up to %s" % (self.table_name, self.timestamp))

    def polling_interval(self, config):
        return config.get('polling_interval') or 1

    def polling_timestamp(self, config):
        return config.get('last_timestamp') or 1495976727


