#!/usr/bin/python

from components.poller import Poller
import logging

def start_conv_poller(config):
    poller = Poller(config, 'votes', 'created_at')
    poller.start()

def load_config():
    return {}

def main():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    config = load_config()
    logger.info(' ----> Loaded settings for running development environment <----')
    logger.info({'SETTINGS': config})

    start_conv_poller(config)

if __name__ == '__main__':
    main()
